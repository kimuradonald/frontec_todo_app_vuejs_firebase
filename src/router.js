import Vue from 'vue'
import VueRouter from 'vue-router'
import TaskList from './components/TaskList.vue'
import Login from './components/Login.vue'
import firebase from 'firebase'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/Tasks',
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/Tasks',
    name: 'tasklist',
    component: TaskList,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/Login',
    name: 'login',
    component: Login
  }
]
 
let router = new VueRouter({
  routes: routes,
  mode: 'history'
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        next()
      } else {
        next({
          path: '/Login',
          query: { redirect: to.fullPath }
        })
      }
    })
  } else {
    next()
  }
})

export default router