import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import firebase from 'firebase'

Vue.config.productionTip = false

// Initialize Firebase
var config = {
  apiKey: "AIzaSyAiRC9Spwg2rTF_-llv--YG0bRJvPa9vZ8",
  authDomain: "todoapp-fec67.firebaseapp.com",
  databaseURL: "https://todoapp-fec67.firebaseio.com",
  projectId: "todoapp-fec67",
  storageBucket: "todoapp-fec67.appspot.com",
  messagingSenderId: "456894435048"
};
firebase.initializeApp(config);

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
